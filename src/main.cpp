#include <iostream>
#include <list>
#include <string>
#include <unistd.h>
#include <ncurses.h>
#include <stdio_ext.h>
#include "gameObject.hpp"
#include "Map.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "bonus.hpp"
#include "collision.hpp"
#include "draw.hpp"


using namespace std;


int main(){

	setlocale (LC_CTYPE, "");

	Map * mapa = new Map();
	mapa->setRange();
	mapa->setNewRange();

	Player * player = new Player();
	Trap * trap = new Trap();
	Bonus * bonus = new Bonus();
	player->setScore(0);
	Draw * draw = new Draw();

	list<Trap> traps;

	list<Trap>::iterator control;

	for (int i = 0; i < 6; i++)
	{
		trap->setSprite(9827);
		trap->movimento(mapa);
		traps.push_front(*trap);
	}
	
	list<Bonus> lBonus;

	list<Bonus>::iterator auxBonus;

	for (int u = 0; u < 6; u++)
	{
		bonus->setSprite(9829);
		bonus->movimento(mapa);
		lBonus.push_front(*bonus);
	}

	Collision * collide = new Collision();

	int auxTrap=0;

	while(player->getWinner() == FALSE && player->getLife() > 0){
	
		initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();
		
		for (control = traps.begin(); control != traps.end(); control++)
		{
			mapa->addElemento(control->getSprite(), control->getPos_x(), control->getPos_y());
			collide->collideTrap(player, mapa);

			if(collide->getColisao() == TRUE){
				player->setPos_x(2);
				player->setPos_y(1);
			}
		}

		for (auxBonus = lBonus.begin(); auxBonus != lBonus.end(); auxBonus++)
		{
			mapa->addElemento(auxBonus->getSprite(), auxBonus->getPos_x(), auxBonus->getPos_y());
				
			collide->collideBonus(player, mapa);

			if(collide->getColisao() == TRUE){
				auxBonus->setSprite(32);	
			}	
			
		}
		
		for (control = traps.begin(); control != traps.end(); control++)
		{
			if(auxTrap == 5){
				control->movimento(mapa);	
			}	
			
		}
		if(auxTrap == 5){
			auxTrap = 0;	
		}
		auxTrap++;
		
		mapa->addElemento(player->getSprite(), player->getPos_x(), player->getPos_y());
		
		draw->drawMap(mapa);
		draw->drawScore(player);

		if(player->getLife() == 0){
			player->setWinner(FALSE);
		}
		else
			collide->collide(player, mapa);
		
		
		
		refresh();
		endwin();
	}

	initscr();
	clear();
	noecho();

	draw->drawFinal(player);
	getch();
	refresh();
	endwin();
	
	return 0;
}

