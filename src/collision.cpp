
#include "collision.hpp"

using namespace std;

Collision::Collision(){
	this->colisao = FALSE;
}

bool Collision::getColisao(){
	return this->colisao;
}

void Collision::setColisao(bool colisao){
	this->colisao = colisao;
}

void Collision::collide(Player *player, Map *map){

	char direcao;

	direcao = getch();
	direcao = tolower(direcao);

	if(direcao == 'w' && map->getPositionRange(player->getPos_y()-1, player->getPos_x()) == ' ') {
		player->setPos_y(player->getPos_y()-1);
	}

	else if(direcao == 's' && map->getPositionRange(player->getPos_y()+1, player->getPos_x()) == ' ') {
		player->setPos_y(player->getPos_y()+1);
	}

	else if(direcao == 'a' && map->getPositionRange(player->getPos_y(), player->getPos_x()-1) == ' ') {
		player->setPos_x(player->getPos_x()-1);
	}

	else if(direcao == 'd' && map->getPositionRange(player->getPos_y(), player->getPos_x()+1) == ' '|| 
		map->getPositionRange(player->getPos_y(), player->getPos_x()+1) == 9830) {

		if(map->getPositionRange(player->getPos_y(), player->getPos_x()+1) == 9830){
			player->setWinner(TRUE);
		}	
			player->setPos_x(player->getPos_x()+1);
	}

	fflush(stdin);	

}

void Collision::collideBonus(Player *player, Map *map){
	
	if(map->getPositionRange(player->getPos_y(), player->getPos_x()) == 9829){
		player->setScore(player->getScore()+15);
		map->addElemento(32, player->getPos_x(), player->getPos_y());
		this->setColisao(TRUE);
	}
	else
		this->setColisao(FALSE);
}

void Collision::collideTrap(Player *player, Map *map){
	
	if(map->getPositionRange(player->getPos_y(), player->getPos_x()) == 9827){
		player->setLife(player->getLife()-20);
		this->setColisao(TRUE);
	}
	else
		this->setColisao(FALSE);
}
