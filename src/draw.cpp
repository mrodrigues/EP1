#include "draw.hpp"
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <stdio_ext.h>

using namespace std;

Draw::Draw(){

}

void Draw::drawMap(Map *mapa){
	mapa->getRange();
}

void Draw::drawScore(Player *player){
	printw("\n\n");
	for (int i = 0; i < 14; i++)
	{
		printw("%lc", 9552);
	}
	printw("%lc", 9559);
	if(player->getScore() == 0){
		printw("\n %lc  Score: %d  %lc\n", 128526, player->getScore(), 9553);		
	}
	else{
		printw("\n %lc  Score: %d %lc\n", 128526, player->getScore(), 9553);
	}

	if (player->getLife() < 100){
		printw(" %lc  Life: %d  %lc\n", 10084, player->getLife(), 9553);	
	}
	else{
		printw(" %lc  Life: %d %lc\n", 10084, player->getLife(), 9553);	
	}
	
	for (int i = 0; i < 14; i++)
	{
		printw("%lc", 9552);
	}
	printw("%lc", 9565);

}

void Draw::drawFinal(Player *player){
	if(player->getWinner() == TRUE){
		printw("Win \n\n");
		printw("Score: %d\n", player->getScore());
	}
	else if(player->getLife() == 0){
		printw("Game Over \n\n");
	}
}