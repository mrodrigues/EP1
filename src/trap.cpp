#include "trap.hpp"
#include <time.h>
#include <stdlib.h>

using namespace std;

Trap::Trap(){

}

Trap::Trap(short sprite, int posx, int posy){
	this->sprite = sprite;
	this->pos_x = posx;
	this->pos_y = posy;
	this->damage = -20;
}

void Trap::setPos_x(int valor){
    this->pos_x = valor;
}

void Trap::setPos_y(int valor){
    this->pos_y = valor;
}

void Trap::setDamage(int damage){
	this->damage = damage;
}

int Trap::getDamage(){
	return this->damage;
}

void Trap::movimento(Map *map){

	int x, y;
	do{
		x = rand() % 37 + 2;
		y = rand() % 25 + 2;
	
		this->setPos_y(y);
		this->setPos_x(x);
	}while(map->getPositionRange(this->getPos_y(), this->getPos_x()) != ' ');
}
