#include "player.hpp"

using namespace std;

Player::Player(){
	this->sprite = 9824;
	this->pos_x = 2;
	this->pos_y = 1;
	this->life = 100;
}

Player::Player(short sprite, int posx, int posy){
	this->sprite = sprite;
	this->pos_x = posx;
	this->pos_y = posy;
	this->life = 100;
}

void Player::setLife(int life){
    this->life = life;
}

void Player::setScore(int score){
    this->score = score;
}

void Player::setWinner(bool winner){
    this->winner = winner;
}

int Player::getLife(){
	return this->life;
}

int Player::getScore(){
	return this->score;
}

bool Player::getWinner(){
	return this->winner;
}


