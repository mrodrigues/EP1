#include "gameObject.hpp"

GameObject::GameObject(){

}

void GameObject::setPos_x(int valor){
    this->pos_x = valor;
}

void GameObject::setPos_y(int valor){
    this->pos_y = valor;
}

void GameObject::setSprite(short sprite){
    this->sprite = sprite;
}

int GameObject::getPos_x(){
    return this->pos_x;
}

int GameObject::getPos_y(){
    return this->pos_y;
}

short GameObject::getSprite(){
    return this->sprite;
}
