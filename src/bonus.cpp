#include "bonus.hpp"
#include <time.h>
#include <stdlib.h>

using namespace std;

Bonus::Bonus(){

}

Bonus::Bonus(short sprite, int posx, int posy){
	this->sprite = sprite;
	this->pos_x = posx;
	this->pos_y = posy;
	this->score = 0;
}

void Bonus::setScore(int score){
	this->score = score;
}

int Bonus::getScore(){
	return this->score;
}

void Bonus::movimento(Map *map){

	int x, y;
	do{
		x = rand() % 37 + 2;
		y = rand() % 25 + 2;
	
		this->setPos_y(y);
		this->setPos_x(x);
	}while(map->getPositionRange(this->getPos_y(), this->getPos_x()) != ' ');
}
