# EP1 - OO (UnB - Gama)

Este projeto consiste em um jogo desenvolvido em C++, onde há interações entre o jogador e os personagens do jogo.

## Instruções de execução:
1.  Instalar a biblioteca NCURSES;

2. Instalar a extensão da biblioteca NCURSES, chamada NCURSESW.

2.1 Bibliotecas compartilhadas para manipulação de terminal (suporte de caracteres de largura);
 - Instalação:
		sudo apt-get install libncursesw5-dev

		
3. Clonar para seu computador o repositório ou baixar de forma compactada;

4. Acessar o diretorio do projeto via terminal;

5. Inserir o comando 'make clean' para limpar os arquivos objeto caso eles existam;

6. Inserir o comando 'make' para compilar o programa;

7. Inserir o comando 'make run' para iniciar o programa;

### Instruções de jogabilidade

1. O jogador está representado pelo caracter naipe-espadas (♠);

2. As armadilhas estão representadas pelo caracter naipe-paus (♣) e por sua vez,
diminuem em 20 a vida do jogador;

3. Os bonus estão representados pelo caracter naipe-copas (♥) e por sua vez,
aumenta a pontuação do jogador em 10 pontos;

4. A vitória se dá ao jogador que chegar ao fim do labirinto, onde se encontra
o caracter naipe-ouros (♦); 

#### Dados do aluno

 - Nome: Matheus Rodrigues do Nascimento
 - Matricula: 16/0015294
 - Repositorio: https://gitlab.com/mrodrigues/EP1


