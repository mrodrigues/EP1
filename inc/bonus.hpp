#ifndef BONUS_HPP
#define BONUS_HPP

#include "gameObject.hpp"
#include "Map.hpp"
#include <iostream>
#include <ncurses.h>

using namespace std;

class Bonus : public GameObject{
	private:
		int score;
	public:
		Bonus();
		Bonus(short sprite, int posx, int posy);
        void setScore(int score);
        int getScore();
        void movimento(Map *map);
       
};

#endif