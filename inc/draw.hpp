#ifndef DRAW_HPP
#define DRAW_HPP

#include "player.hpp"
#include "Map.hpp"
#include <iostream>
#include <ncurses.h>

using namespace std;

class Draw {
	private:

	public:
	Draw();

	void drawMap(Map *mapa);
	void drawScore(Player *player);
	void drawFinal(Player *player);
};
#endif
