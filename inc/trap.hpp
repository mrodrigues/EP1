#ifndef TRAP_HPP
#define TRAP_HPP

#include "gameObject.hpp"
#include "Map.hpp"
#include <iostream>
#include <ncurses.h>

using namespace std;

class Trap : public GameObject{
	private:
		int damage;
	public:
		Trap();
		Trap(short sprite, int posx, int posy);
		void setPos_x(int valor);
        void setPos_y(int valor);

        void setDamage(int damage);
        int getDamage();
        
		void movimento(Map *map);
};
#endif