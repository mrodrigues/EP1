#ifndef MAP_H
#define MAP_H

#include <iostream>
#include <string>

class Map{

	private:
		short range[28][41];
		short newRange[28][41];
	public:
		Map();
		
		void setRange();
		void setNewRange();
		void getRange();

		short getPositionRange(int posy, int posx);
		void addElemento(short sprite, int posx, int posy);

};

#endif