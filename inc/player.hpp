#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "gameObject.hpp"
#include <iostream>
#include <ncurses.h>

using namespace std;

class Player : public GameObject{
	private:
		int life;
		int score;
		bool winner;
	public:
		Player();
		Player(short sprite, int posx, int posy);
		void setLife(int life);
		void setScore(int score);
		void setWinner(bool winner);
		int getLife();
		int getScore();
		bool getWinner();
};
#endif