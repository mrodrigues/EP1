#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <iostream>

using namespace std;

class GameObject{
    protected:
        short sprite;
        int pos_x;
        int pos_y;
    public:
        GameObject();
        void setPos_x(int valor);
        void setPos_y(int valor);
        void setSprite(short sprite);
        int getPos_x();
        int getPos_y();
        short getSprite();
};
#endif