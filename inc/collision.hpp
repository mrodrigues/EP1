#ifndef COLLISION_HPP
#define COLLISION_HPP

#include <iostream>
#include <list>
#include <ncurses.h>
#include "gameObject.hpp"
#include "bonus.hpp"
#include "Map.hpp"
#include "player.hpp"
#include "trap.hpp"

class Collision{
	private:
		bool colisao;
	public:
		Collision();

		void setColisao(bool colisao);
		bool getColisao();

		void collide(Player *player, Map *map);
		void collideBonus(Player *player, Map *map);
		void collideTrap(Player *player, Map *map);
		
};

#endif